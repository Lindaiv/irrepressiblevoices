-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Erstellungszeit: 10. Feb 2013 um 15:47
-- Server Version: 5.5.27
-- PHP-Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Datenbank: `ivoices`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `blog_articles`
--

CREATE TABLE IF NOT EXISTS `blog_articles` (
  `article_id` int(11) NOT NULL AUTO_INCREMENT,
  `article_date` datetime NOT NULL,
  `article_title` varchar(255) NOT NULL,
  `article_content` text NOT NULL,
  `article_author` int(11) NOT NULL,
  `article_image` varchar(255) DEFAULT NULL,
  `article_secimage` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`article_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Daten für Tabelle `blog_articles`
--

INSERT INTO `blog_articles` (`article_id`, `article_date`, `article_title`, `article_content`, `article_author`, `article_image`, `article_secimage`) VALUES
(1, '2013-02-10 15:15:15', 'Irrepressible Voices is live', '"What did I commit myself to?" our programmer asked half-seriously.<br />\r\n\r\nThis weekend Irrepressible Voices took part in <a href="http://berlin.givecamp.de/en/">Give Camp Berlin</a>. For a whole weekend programmers, developers and designers came together to donate their talent and skills to Non-Profit-Projects or Organisations.This event was graciously hosted by <a href="http://www.rally.org">Rally.org</a>, the social incubator for projects with a good cause.<br />\r\n<br />\r\nThe event started with all participating Non Profits giving a  short presentation. We then formed teams and went straight to work!  Irrepressible Voices would like to thank our new team members who have done an amazing job in supporting us throughout the Give Camp. From re-designing the whole website to a user friendly interface (*thanks to our brilliant social interaction designer*) to elaborating a tool for secure video uploads (*thanks to our amazing developer*). \r\nThe weekend was a whole success. We have made amazing progress in defining how the platform will look like and operate in the future.<br />\r\n\r\nNot only have we won new friends, we also got inspiring advices and valuable tips for making \r\nthe call to action to enforce human rights an adventurous journey.<br />\r\n\r\n<a href="http://www.oezgueng.com" target="_blank">&Ouml;tzi</a>, our developer, became  so fond about our platform that he immediately signed up as the first registered <i>irrepressible voice</i> on <a href="http://www.Irrepressiblevoices.org">Irrepressible Voices</a> \r\n\r\nCheck out the blog for more regular updates on Irrepressible Voices.', 1, 'givecamp.jpeg', 'rallyLogo.png');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `uploads`
--

CREATE TABLE IF NOT EXISTS `uploads` (
  `upload_id` int(11) NOT NULL AUTO_INCREMENT,
  `upload_date` datetime NOT NULL,
  `upload_recorded` date NOT NULL,
  `upload_active` enum('0','1') NOT NULL DEFAULT '0',
  `upload_userid` int(11) NOT NULL,
  `upload_place` varchar(255) DEFAULT NULL,
  `upload_title` varchar(255) NOT NULL,
  `upload_filepicker_url` text,
  `upload_filepicker_filename` varchar(255) DEFAULT NULL,
  `upload_filepicker_mimetype` varchar(255) DEFAULT NULL,
  `upload_filepicker_size` int(11) DEFAULT NULL,
  `upload_published` varchar(255) DEFAULT NULL,
  `upload_published_date` datetime DEFAULT NULL,
  `upload_tags` text,
  `upload_desc` text,
  `upload_issue` text,
  `upload_statement` text,
  PRIMARY KEY (`upload_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `upload_comments`
--

CREATE TABLE IF NOT EXISTS `upload_comments` (
  `comment_id` int(11) NOT NULL AUTO_INCREMENT,
  `comment_uploadid` varchar(255) NOT NULL,
  `comment_userid` int(11) NOT NULL,
  `comment_date` datetime NOT NULL,
  `comment_message` text NOT NULL,
  PRIMARY KEY (`comment_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `upload_verifying`
--

CREATE TABLE IF NOT EXISTS `upload_verifying` (
  `verify_id` int(11) NOT NULL AUTO_INCREMENT,
  `verify_uploadid` varchar(200) NOT NULL,
  `verify_date` datetime NOT NULL,
  `verify_userid` int(11) NOT NULL,
  PRIMARY KEY (`verify_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_username` varchar(255) NOT NULL,
  `user_email` varchar(255) DEFAULT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_register_date` datetime NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Daten für Tabelle `users`
--

INSERT INTO `users` (`user_id`, `user_username`, `user_email`, `user_password`, `user_register_date`) VALUES
(1, 'oezgueng', 'oezgueng@me.com', 'cc03e747a6afbbcbf8be7668acfebee5', '0000-00-00 00:00:00');
