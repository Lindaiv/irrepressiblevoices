<?php
/**
 * blogger@collaboratory.de
 * blogger2012
 */
class SubmitController extends Zend_Controller_Action
{
    public $_categories;

    public function init()
    {
        $this->_categories = array(
            'Death penalty',
            'Armed Conflict',
            'Arms control',
            'Childrens Rights',
            'Detention and Imprisonment',
            'Discrimination',
            'Economic, Social and Cultural Rights',
            'Enforced Disappearances',
            'Freedom of Expression',
            'Human Rights Defenders',
            'Human Rights Education',
            'Impunity',
            'Indigenous Peoples',
            'Poverty',
            'Refugees, Migrants and Internally Displaced Persons',
            'Religion',
            'Torture'
        );
    }

    public function indexAction()
    {
        // action body
    }

    public function uploadAction()
    {
        $login = new Zend_Session_Namespace('login');
        if($login->user_id) {
            $params = $this->getRequest()->getParams();

            if(empty($params['title']) || empty($params['place'])) {
                $this->_helper->redirector('index', 'submit');
            }

            $db = Zend_Registry::get('db');

            $data = array(
                'upload_date' => new Zend_Db_Expr('NOW()'),
                'upload_active' => '0',
                'upload_userid' => $login->user_id,
                'upload_title' => $params['title'],
                'upload_recorded' => $params['recorded'],
                'upload_place' => $params['place'],
                'upload_filepicker_url' => $params['filepicker_url'],
                'upload_filepicker_filename' => $params['filepicker_filename'],
                'upload_filepicker_mimetype' => $params['filepicker_mimetype'],
                'upload_filepicker_size' => $params['filepicker_size'],
            );


            $db->insert('uploads', $data);

            // redirect if success
            $this->_helper->flashMessenger->addMessage(array('success'=>'Danke'));
            $this->_helper->redirector(
                'success', 'submit', null,
                array('upload_id' => $db->lastInsertId()));
        }
    }
    public function successAction() {

        $uploadId = $this->getRequest()->getParam('upload_id');
        $this->view->upload_id = $uploadId;

        $db = Zend_Registry::get('db');

        if($this->getRequest()->getMethod() == 'POST') {
            $params = $this->getRequest()->getParams();

            // data
            $updateArr = array(
                'upload_tags' => $params['tags'],
                'upload_desc' => $params['desc'],
                'upload_issue' => $params['issue'],
                'upload_statement' => $params['statement']
            );
            $db->update('uploads', $updateArr, 'upload_id = '.$uploadId);

            // redirect if success
            $this->_helper->redirector('success', 'submit');
        }
        $this->view->categories = $this->_categories;
    }
}
