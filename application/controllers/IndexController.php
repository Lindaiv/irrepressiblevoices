<?php

class IndexController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        // action body
        $db = Zend_Registry::get('db');
        $select = $db->select();
        $select->from('blog_articles');
        $select->order('article_id DESC');
        $select->limit(1);
        $result = $db->fetchRow($select);

        // result
        $this->view->firstBlog = $result;

    }

    /**
     * Login/Registrieren Action
     */
    public function loginAction()
    {
        $fields = $this->getRequest()->getParams();

        if(isset($fields['register'])){
            $this->register($fields['register']);
        }
        if(isset($fields['register_anon'])){
            $this->register($fields['register_anon'], true);
        }

        if(isset($fields['login'])){
            $this->login($fields['login']);
        }


    }

    public function logoutAction() {
        $login = new Zend_Session_Namespace('login');
        unset($login->user_id);

        $this->_helper->flashMessenger->addMessage(array('success'=>'You are now logged out.'));
        $this->_helper->redirector('index', 'index');
    }

    /**
     * Login
     */
    private function login($fields)
    {
        if(empty($fields['username']) || empty($fields['password'])) {
            $this->_helper->flashMessenger->addMessage(array('error'=>
                'Please check that you have filled in all the necessary information.'
            ));
            $this->_helper->redirector('login', 'index');
        }

        $db = Zend_Registry::get('db');
        $select = $db->select();
        $select->from('users');
        $select->where('user_username = ?', $fields['username']);
        $select->where('user_password = ?', md5($fields['password']));
        $select->limit(1);

        $result = $db->fetchAll($select);
        if(count($result) == 0) {
            $this->_helper->flashMessenger->addMessage(array('error'=>'Account not found.'));
            $this->_helper->redirector('login', 'index');
        } elseif(count($result) >= 1) {

            $login = new Zend_Session_Namespace('login');
            $login->user_id = $result[0]['user_id'];

            $this->_helper->flashMessenger->addMessage(array('success'=>'You are logged in now.'));
            $this->_helper->redirector('index', 'index');
        }

    }
    /**
     * Register
     */
    private function register($fields, $anon = false)
    {

        if($anon == true) {
            $fields['email'] = NULL;
        }

        if(empty($fields['username']) || empty($fields['password']) || empty($fields['password2'])) {
            $this->_helper->flashMessenger->addMessage(
                array('error'=>'Please check that you have filled in all the necessary information.')
            );
            $this->_helper->redirector('login', 'index');
        }
        if($fields['password'] != $fields['password2']) {
            $this->_helper->flashMessenger->addMessage(array('error'=>'Your Passwords did not match.'));
            $this->_helper->redirector('login', 'index');
        }

        $db = Zend_Registry::get('db');

        $data = array(
            'user_username'     => $fields['username'],
            'user_email'        => $fields['email'],
            'user_password'     => md5($fields['password']),
            'user_register_date' => new Zend_Db_Expr('NOW()')
        );

        $db->insert('users', $data);

        $login = new Zend_Session_Namespace('login');
        $login->user_id = $db->lastInsertId();

        $this->_helper->flashMessenger->addMessage(array('success'=>'Thank you for registration.'));
        $this->_helper->redirector('index', 'index');
    }


}

