<?php

class BlogController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        $this->view->blogitems = $this->getBlogItems();
    }
    public function detailAction()
    {
        $articleId = $this->getRequest()->getParam('articleid');
        if(empty($articleId)) {
            $this->_helper->flashMessenger->addMessage(array('error'=>'Artikel anzeige ist nicht möglich.'));
            $this->_helper->redirector('index', 'index');
        }
        $db = Zend_Registry::get('db');
        $select = $db->select();
        $select->from(array('blog' => 'blog_articles'));
        $select->where('blog.article_id = ?', $articleId);
        $select->join(array('user' => 'users'), 'blog.article_author = user.user_id');

        $this->view->article = $db->fetchRow($select);
    }

    /**
     * get Blog Items from table blog_articles
     */
    private function getBlogItems()
    {
        $db = Zend_Registry::get('db');
        $select = $db->select();
        $select->from(array('blog' => 'blog_articles'));
        $select->join(array('user' => 'users'), 'blog.article_author = user.user_id');
        $select->order('blog.article_id DESC');

        return $db->fetchAll($select);
    }

    /**
     * get
     */
}
